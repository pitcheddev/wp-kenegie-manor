<?php
/* Template name: Locals Page */
get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
			<div class="row">
				<div class="col-lg-9">
					<h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
					
					<?php //Locals Voucher download section
					if(have_rows('vouchers')) { ?>
						<h2><?php the_field('voucher_section_title'); ?></h2>
						<?php the_field('voucher_section_desc');
							// loop through vouchers
						while(have_rows('vouchers')) { the_row(); ?>
							<blockquote class="download">
								<h3><?php the_sub_field('voucher_title'); ?></h3>
								<?php the_sub_field('voucher_description'); ?>
								<a class="btn-violet" href="<?php echo the_sub_field('voucher_download'); ?>" target="_blank">Click to download voucher</a>
							</blockquote>    
					<?php }
					} ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>