<?php get_header(); the_post(); ?>
<section class="inner-page">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-9 col-lg-9">
          <div class="content-wrapper error-page">
            <h1><span>Ooops!</span> This page seems to have packed up and gone on holiday...</h1>
            <p>Sorry you kind find what you're looking for, the page you requested may have been moved or removed from the website. Use the menu above to find what you're looking for or follow the link below to return to the home page.</p>
            <p class="aligncenter"><a href="/" class="btn-violet">Click to return to our home page</a></p>
          </div>
        </div>
        <?php get_template_part('booking') ?>
      </div>
    </div>
  </section>
<?php get_footer(); ?>