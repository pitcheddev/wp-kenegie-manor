<?php
/* Template name: Tariff Page */
get_header(); the_post(); ?>

	<section class="tariff">
        <div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 text-center">
					<h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="accommodation">
				<div class="accom-slider">
					<div class="mask-holder">
						<div class="mask">
							<div class="slideset" data-slick='{"autoplay": false}'>
								<?php if( have_rows('unit_list') ): 
									while( have_rows('unit_list') ): the_row(); 
									$image = get_sub_field('unit_image');
								?>
									<article class="accom">
										<img src="<?php echo $image['sizes']['home-accom']; ?>" alt="<?php echo $image['alt']; ?>">
										<h3><?php the_sub_field('unit_title') ?></h3>
										<span><?php the_sub_field('unit_sub_title') ?></span>
										<?php the_sub_field('unit_description') ?>
										<?php if(get_sub_field('unit_link')){
											$label = get_sub_field('unit_link_label') ? get_sub_field('unit_link_label') : 'View The Range'; ?>
											<a href="<?php the_sub_field('unit_link') ?>" class="btn outline violet"><?= $label ?></a>
										<?php } ?>
									</article>
								<?php endwhile;
								endif; ?>
							</div>  
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9">
					<?php
					$count = 0;
					if( have_rows('tp_tabletab_list') ){
						while ( have_rows('tp_tabletab_list') ) { the_row();
						$count++;
						$getColor = get_sub_field('tp_tablist_color');
						$color = "";
						if($getColor != 'default'){
							$color = $getColor;
						}
					?>
						<div class="accord-header <?= $color; ?>">
							<a data-toggle="collapse" href="#collapse_<?= $count ?>" data-parent="#content" aria-expanded="true" aria-controls="collapse_<?= $count ?>">
								<div class="card-header close-content" role="tab" id="heading_<?= $count ?>">
									<div class="text">
										<?php if(get_sub_field('tp_tablist_small_text')) { ?>
										<div class="row">
											<div class="col-3 col-md-6">
												<h2 class="mb-0"><?php the_sub_field('tp_tablist_title') ?></h2>
											</div>
											<div class="col-9 col-md-6">
												<div class="sm-txt pr-4 pr-md-0">
													<?php the_sub_field('tp_tablist_small_text') ?>
												</div>
											</div>
										</div>
										<?php }
										else { ?>
											<h2 class="mb-0"><?php the_sub_field('tp_tablist_title') ?></h2>
										<?php } ?>
									</div>
									<i class="fal fa-angle-down"></i>
								</div>
							</a>
							<div id="collapse_<?= $count ?>" class="collapse" role="tabpanel" aria-labelledby="heading_<?= $count ?>">
								<div class="card-body">
									<?php
										$getTable = get_sub_field('tp_tablist_select_table');
										echo do_shortcode('[table id='.$getTable.' /]');
									?>
								</div>
							</div>
						</div>
					<?php
						}
					}?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>