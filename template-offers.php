<?php
/* Template name: Special Offers Page */
get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
	
                    
                    <div class="offers-page-wrapper">
						<div class="asbo"></div>
						<?php
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						query_posts(
							array(
							  'post_type' => 'special_offer',
							  'posts_per_page' => 12,
							  'paged' => $paged,
							  'meta_query' => array(
									array(
										'key' => 'offer_type',
										'value' => '"general"',
										'compare' => 'LIKE'
									)
								)
						));
						$count = 0;
						if (have_posts()) {
							while (have_posts()) { the_post();
							//vars
							$count++;
							$featured = get_field('offer_featured');
							$image = get_field('offer_image');
							$promo = get_field('offer_promo');
							
						?>
								<div class="offer">
								
									<?php if(get_field('offer_flash_title') || get_field('offer_flash_price')) { ?>
									<div class="price-flash"><?php the_field('offer_flash_title'); ?> <span><?php the_field('offer_flash_price'); ?></span></div>
									<?php }
									if($featured) { ?>
									<div class="sun-flash"></div>
									<?php } ?>
									
									<div class="row">
										<div class="col-md-5">
											<img src="<?php echo $image['sizes']['offer-list']; ?>" alt="<?php echo $image['alt']; ?>">
										</div>
										<div class="col-md-7">
											<div class="content">
												<h2><?php the_title(); ?></h2>
												<h4><?php the_field('offer_sub_heading'); ?></h4>
												<p><?php the_field('offer_sub_description'); ?></p>
												<?php the_field('offer_description'); ?>
												<a href="https://kenegie.searchbreaks.com"  onclick="fbq('track', 'InitiateCheckout');" class="btn solid light-violet">Check prices and book</a>
												
												<?php if($promo) { ?>
													<div class="discount-code">Discount code: <span><?php echo $promo; ?></span></div>
												<?php } ?>
											</div>
										</div>
									</div>                        
								</div>
								<a href="#" id="full-details" class="full-details close-details" data-toggle="collapse" data-target="#details_<?= $count?>">Expand to view full details <i class="fal fa-angle-down"></i></a>
								<div class="details-wrapper collapse" id="details_<?= $count?>">
									<p><strong>Terms and Conditions:</strong> <?php the_field('offer_terms_and_conditions'); ?></p>
								</div>
						<?php	}
							}
							wp_reset_query();
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>