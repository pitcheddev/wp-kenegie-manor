<?php get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
			<div class="row">
				<div class="col-lg-9">
					<h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>