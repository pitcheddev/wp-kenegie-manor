<?php

include(TEMPLATEPATH . '/functions/register-acf.php');
include(TEMPLATEPATH . '/functions/register-logo.php');
include(TEMPLATEPATH . '/functions/register-options.php');
include(TEMPLATEPATH . '/functions/register-user-roles.php');
include(TEMPLATEPATH . '/functions/register-owners-portal.php');
include(TEMPLATEPATH . '/functions/register-hooks.php');

/* Set up actions */
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 247, 229, true );
add_image_size( 'home-slider', 1300, 659, true );
add_image_size( 'home-accom', 309, 236, true );
add_image_size( 'home-feature', 311, 266, true );
add_image_size( 'post-img', 247, 229, true );
add_image_size( 'post-sidebar', 237, 165, true );
add_image_size( 'page-banner', 848, 446, true );


add_image_size( 'home-slider-lg', 1500, 500, true );
add_image_size( 'home-slider-md', 990, 500, true );
add_image_size( 'home-slider-sm', 768, 500, true );


add_image_size( 'home-slider-md_2', 990, 519, true );
add_image_size( 'home-slider-sm_2', 768, 576, true );
add_image_size( 'home-slider-xs', 480, 312, true );


add_image_size( 'accom-list', 555, 380, true );
add_image_size( 'accom-gallery', 243, 163, true );

add_image_size( 'offer-list', 350, 240, true );

add_image_size( '3col-img', 337, 284, true );

add_action( 'after_setup_theme', 'kenegie_setup' );

add_action('init', 'create_specialoffers_posttype', 0);
add_action('init', 'create_reviews_posttype', 0);
add_action('init', 'create_accommodation_posttype', 0);

function kenegie_setup()
{
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );

    register_nav_menus( array('main-menu' => __('Main Menu' , 'default'),
                             'mobile-menu' => __('Mobile Menu' , 'default'),
                             'quick-links' => __('Quick Links' , 'default'),
                             'important-info' => __('Important Info', 'default')
                             )
    );

}
/* Load Assets */
add_action( 'wp_enqueue_scripts', 'kenegie_load_assets' );

function kenegie_load_assets()
{
	wp_register_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array('jquery'), false, true);
	wp_register_script('slick_carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), false, true);
	wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), false, true);
    wp_register_script('mmenu', get_template_directory_uri() . '/js/jquery.mmenu.all.min.js', array('jquery'), false, true);
    wp_register_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), false, true);
    wp_register_script('main', get_template_directory_uri() . '/js/jquery.main.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), false, true);
	//wp_register_script('pitchedbooking', 'https://bookings.kenegie-manor.co.uk/scripts/pitchedwidget.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), false, true);
	 wp_register_script('pitchedbooking', get_template_directory_uri() . '/js/pitchedwidget.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), false, true);
  

    wp_register_style('mmenu_css', get_template_directory_uri() . '/css/jquery.mmenu.all.min.css');
    wp_register_style('style_css', get_template_directory_uri() . '/css/style.css');
    wp_register_style('style', get_template_directory_uri() . '/style.css');
	wp_register_style('datepicker', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css');

    wp_enqueue_script('bootstrap');
    wp_enqueue_script('slick_carousel');
    wp_enqueue_script('popper');
    wp_enqueue_script('mmenu');
    wp_enqueue_script('slick');
	wp_enqueue_script('main');

    wp_enqueue_style('mmenu_css');
	wp_enqueue_style('style_css');
    wp_enqueue_style('style');
	wp_enqueue_style('datepicker');
}
/* Menu Functions */
function kenegie_get_header_menu() {

    wp_nav_menu(array(
      'theme_location' => 'main-menu',
      'container'       => '',
      'container_id' => '',
      'container_class' => '',
      'items_wrap'      => '<ul>%3$s</ul>',
      'link_before' => '',
      'link_after' => '',
      'walker'  => new kenegie_walker_nav_menu()
    ));

}

function kenegie_get_mobile_menu() {

    wp_nav_menu(array(
      'theme_location' => 'mobile-menu',
      'container'       => '',
      'container_id' => '',
      'container_class' => '',
      'items_wrap'      => '<ul class="sub-mainav">%3$s</ul>',
      'link_before' => '',
      'link_after' => '',
      'walker'  => new kenegie_walker_nav_menu()
    ));

}

function kenegie_quick_links_menu() {

    wp_nav_menu(array(
      'theme_location' => 'quick-links',
      'container'       => '',
      'container_id' => '',
      'container_class' => '',
      'items_wrap'      => '<ul>%3$s</ul>',
      'link_before' => '',
      'link_after' => ''
    ));

}

function kenegie_important_information() {

wp_nav_menu(array(
    'theme_location' => 'important-info',
    'container' => '',
    'container_class' => '',
    'container_id' => '',
    'items_wrap'  => '<ul>%3$s</ul>',
    'link_before' => '',
    'link_after' => ''
));

}

class kenegie_walker_nav_menu extends Walker_Nav_Menu {

    // add classes to ul sub-menus
    function start_lvl( &$output, $depth = 0, $args = array()) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0

        // build html
        $output .= "\n" . $indent . '<div class="dropdown" role="menu"><ul>' . "\n";
    }

    // add main/sub classes to li's and links
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? '' : '' ),
            ( $depth >=2 ? '': '' ),
            'dropdown menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        // build html
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '">';

        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $link = '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s';


        if($depth == 1)
        {
            $link = '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s';

        }

        if($item->url == "#")
        {
            $attributes .= 'class="dropdown-toggle"';
            $attributes .= 'data-toggle="dropdown"';
            $attributes .= 'role="button"';
            $attributes .= 'aria-expanded="false"';


        }

        $item_output = sprintf( $link,
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,

            $args->after
        );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_lvl(&$output, $depth = 0, $args = NULL)
      {
         $indent = str_repeat("\t", $depth);
         $output .= "$indent</ul></div>\n";
      }
}
/* Set up post types */
function create_specialoffers_posttype() {
	$labels = array(
		'name'               => _x( 'Special Offers', 'post type general name'),
		'singular_name'      => _x( 'Special Offer', 'post type singular name'),
		'menu_name'          => _x( 'Special Offers', 'admin menu' ),
		'name_admin_bar'     => _x( 'Special Offer', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'offer' ),
		'add_new_item'       => __( 'Add New Offer'),
		'new_item'           => __( 'New Offer' ),
		'edit_item'          => __( 'Edit Offer'),
		'view_item'          => __( 'View Offer'),
		'all_items'          => __( 'All Offers'),
		'search_items'       => __( 'Search Offers'),
		'parent_item_colon'  => __( 'Parent Offers:'),
		'not_found'          => __( 'No Offers found.'),
		'not_found_in_trash' => __( 'No Offers found in Trash.')
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
        'menu_icon'          => 'dashicons-star-filled',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'offers'),
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
        );

	register_post_type( 'special_offer', $args );


}

function create_reviews_posttype() {
	$labels = array(
		'name'               => _x( 'Reviews', 'post type general name'),
		'singular_name'      => _x( 'Review', 'post type singular name'),
		'menu_name'          => _x( 'Reviews', 'admin menu' ),
		'name_admin_bar'     => _x( 'Reviews', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'review' ),
		'add_new_item'       => __( 'Add New Review'),
		'new_item'           => __( 'New Review' ),
		'edit_item'          => __( 'Edit Review'),
		'view_item'          => __( 'View Review'),
		'all_items'          => __( 'All Reviews'),
		'search_items'       => __( 'Search Reviews'),
		'parent_item_colon'  => __( 'Parent Reviews:'),
		'not_found'          => __( 'No Reviews found.'),
		'not_found_in_trash' => __( 'No Reviews found in Trash.')
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
    'menu_icon'          => 'dashicons-format-status',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'reviews'),
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
  );

	register_post_type( 'review', $args );

}
function create_accommodation_posttype() {
	$labels = array(
		'name'               => _x( 'Accommodation', 'post type general name'),
		'singular_name'      => _x( 'Accommodation', 'post type singular name'),
		'menu_name'          => _x( 'Accommodation', 'admin menu' ),
		'name_admin_bar'     => _x( 'Accommodation', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'unit' ),
		'add_new_item'       => __( 'Add New Unit'),
		'new_item'           => __( 'New Unit' ),
		'edit_item'          => __( 'Edit Unit'),
		'view_item'          => __( 'View Unit'),
		'all_items'          => __( 'All Units'),
		'search_items'       => __( 'Search Units'),
		'parent_item_colon'  => __( 'Parent Units:'),
		'not_found'          => __( 'No Units found.'),
		'not_found_in_trash' => __( 'No Units found in Trash.')
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
    'menu_icon'          => 'dashicons-admin-multisite',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'self-catering'),
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
        );

	register_post_type( 'accommodation', $args );


}

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );
function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
/* Set Excerpts */
function excerpt($num) {
    $limit = $num+1;
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt)." ...";
    echo $excerpt;
}
/* Review Block Shortcode */
function pitched_review_block() {
    ob_start();
    get_template_part('review-block');
    $content = ob_get_clean();
    return $content;
}
add_shortcode('review-block' , 'pitched_review_block' );
/* Display Pet Friendly Units */
function pitched_pet_units() {
    ob_start();
    get_template_part('pet-units');
    $content = ob_get_clean();
    return $content;
}
add_shortcode('pet-units' , 'pitched_pet_units' );

//Pagination
function pagination_nav() {
	global $wp_query;

	$big = 999999999; // need an unlikely integer

	$arr = paginate_links( array(
		'mid_size'  => 2, 
		'type' => 'array',
		'prev_text' => '<i class="fal fa-angle-left"></i> Previous',
		'next_text' => 'Next <i class="fal fa-angle-right"></i>'));
	$pagination = '<ul>';
	for($x = 0; $x < count($arr); $x++)
	{
		$pagination .= "<li>".$arr[$x]."</li>";
	}
	$pagination .= '</ul>';
	echo $pagination;
}

//[button link = "facebook.com" label = "Facebook" isnewtab = "true"][/button]
add_shortcode('button', 'pitched_button');
function pitched_button($atts) {
	extract(shortcode_atts(array(
		'isnewtab' => 'false',
		'link' => '',
		'label' => '',
	), $atts));
	
	if($isnewtab == 'true'){
		$shortcode = '<a href="'.$link.'" class="btn solid light-violet shadow" tabindex="0" target = "_blank">'.$label.'</a>';
	}else{
		$shortcode = '<a href="'.$link.'" class="btn solid light-violet shadow" tabindex="0">'.$label.'</a>';
	}
	
	return $shortcode;
}