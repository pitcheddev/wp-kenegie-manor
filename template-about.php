<?php
/* Template name: About Page */
get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
					
					<?php
					$tab = "";
					$tabList = "";
					$content = "";
					$count = 0;
					if( have_rows('tab_list') ){
						while ( have_rows('tab_list') ) { the_row();
							$class = "";
							$class2 = "";
							$class3 = "";
							if($count == 0){
								$class = "active";
								$class2 = " show active";
							}
							$title = get_sub_field('tab_list_title');
							$icon = '';
							if($title == 'Facilities'){
								$icon = '<span class="icon"></span>';
								$class3 = "facilities";
							}
							else if($title == 'Activities'){
								$icon = '<i class="fas fa-swimmer"></i>';
								$class3 = "activities";
							}
							else if($title == 'Explore'){
								$icon = '<i class="fas fa-binoculars"></i>';
								$class3 = "explore";
							}
							else if($title == 'Places'){
								$icon = '<i class="fas fa-map-marked-alt"></i>';
								$class3 = "places";
							}
							else if($title == 'Things to do'){
								$icon = '<i class="fas fa-camera-retro"></i>';
								$class3 = "things_to_do";
							}
							else if($title == 'Beaches'){
								$icon = '<i class="fas fa-umbrella-beach"></i>';
								$class3 = "beaches";
							}
							$tab .= '<li class="nav-item">
								  <a id="tab_'.$count.'" href="#pane_'.$count.'" class="nav-link '.$class.'  '.$class3.'" data-toggle="tab" role="tab">'.$icon.' '.$title.'</a>
								  
							  </li>';
							$tabList .= '<option value="'.$count.'" class="ct">'.$title.'</option>';
							
							$content .= '<div id="pane_'.$count.'" class="card tab-pane fade '.$class2.'" role="tabpanel" aria-labelledby="tab_'.$count.'">
								<div role="tabpanel" aria-labelledby="heading_'.$count.'">
									<div class="card-body">
										<div class="row justify-content-center">
											<div class="col-lg-10">
												 '.get_sub_field('tab_list_content').'
											</div>
										</div> ';
							if( have_rows('tab_list_3colbox') ){
								$content .= '<div class="columns">
											<div class="row">';
								while ( have_rows('tab_list_3colbox') ) { the_row();
									$image = get_sub_field('tab_list_3colbox_image');
									$content .= '<div class="col-sm-6 col-lg-4 my-3">
										<div class="column w-btn h-100">
											<img src="'.$image['sizes']['3col-img'].'" alt="'.$image['alt'].'">
											<div class="content">
												<h3>'.get_sub_field('tab_list_3colbox_title').'</h3>
												'.get_sub_field('tab_list_3colbox_description').'
											</div>';
									if(get_sub_field('tab_list_3colbox_button_link') && get_sub_field('tab_list_3colbox_button_text')){
										$content .= '<a href="'.get_sub_field('tab_list_3colbox_button_link').'" class="btn solid white">'.get_sub_field('tab_list_3colbox_button_text').'</a>';
									}
									$content .= '</div>
									</div>';
								}
								$content .= '</div>
										</div>';
							}
							$content .= '</div>
								</div>
							</div>';
							
							$count++;
						}
					}?>

					<div class="tab-wrapper">
					  <ul id="tabs" class="nav nav-tabs nav-justified" role="tablist">
						  <?php echo $tab; ?>
					  </ul>
					</div>

					<div class="filter-select">
					  <select class="d-lg-none" id="tab_selector">
						<option class="ct" value="" disabled selected>Filter</option>
						<?php echo $tabList; ?>
					  </select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div id="content" class="tab-content" role="tablist">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>