<?php
  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
  query_posts(
	array(
	  'post_type' => 'accommodation',
	  'posts_per_page' => -1,
	  'paged' => $paged,
	  'meta_query' => array(
		  array(
			  'key' => 'unit_grades',
			  'value' => '"pets"',
			  'compare' => 'LIKE'
		  )
	  )
	)
  );
  if (have_posts()) :
  while (have_posts()) :
  the_post();
  //vars
  $unitpricelow = get_field('unit_price_low');
  $unitpricehigh = get_field('unit_price_high');
  $unitsize = get_field('unit_size');
  $unitdescription = get_field('unit_short_description');
?>
<!-- Unit loop -->
  <div class="accommodation">
	<div class="price">
	  Price range from &pound;<?php echo $unitpricelow . ' to &pound;' . $unitpricehigh; ?>
	</div>
	<div class="row">
	  <div class="col-sm-4">
		 <?php the_post_thumbnail('post-img'); ?>
	  </div>
	  <div class="col-sm-8">
		<div class="details">
		  <h2><?php the_title(); ?></h2>
		  <div class="title">
			<span><?php echo $unitsize; ?></span>
			<ul class="award-medals">
			  <?php
				$unitgrades = get_field('unit_grades');
				  if( in_array('bronze', $unitgrades) ) {
					echo '<li><img src="', get_template_directory_uri(), '/img/bronze.png" /></li>';
				  }
				  if( in_array('silver', $unitgrades) ) {
					echo '<li><img src="', get_template_directory_uri(), '/img/silver.png" /></li>';
				  }
				?>
			</ul>
		  </div>
		  <?php echo $unitdescription; ?>
		  <a href="<?php the_permalink(); ?>" class="btn-violet">Click for full details</a>
		</div>
	  </div>
	</div>
  </div>

  <?php endwhile;
	  endif;
wp_reset_query();
?>