(function ($) {
    //Create plugin named Setcase
    $.fn.Setcase = function (settings) {
        // Defaults
        var config = {
            caseValue: 'Upper',
            changeonFocusout: false
        };

        //Merge settings
        if (settings) $.extend(config, settings);

        this.each(function () {
            //keypress event
            if (config.changeonFocusout == false) {
                $(this).keypress(function () {
                    if (config.caseValue == "upper") {
                        var currVal = $(this).val();
                        //console.log(currVal);
                        //console.log(currVal.toUpperCase());
                        $(this).val(currVal.toUpperCase());
                    }
                    else if (config.caseValue == "lower") {
                        var currVal = $(this).val();
                        $(this).val(currVal.toLowerCase());
                    }
                    else if (config.caseValue == "title") {
                        var currVal = $(this).val();
                        $(this).val(currVal.charAt(0).toUpperCase() + currVal.slice(1).toLowerCase());
                    }
                    else if (config.caseValue == "pascal") {
                        var currVal = $(this).val();
                        currVal = currVal.toLowerCase().replace(/\b[a-z]/g, function (txtVal) {
                            return txtVal.toUpperCase();
                        });
                        $(this).val(currVal);
                    }
                });
            }
            //blur event		
            $(this).blur(function () {
                if (config.caseValue == "upper") {
                    var currVal = $(this).val();
                    $(this).val(currVal.toUpperCase());
                }
                else if (config.caseValue == "lower") {
                    var currVal = $(this).val();
                    $(this).val(currVal.toLowerCase());
                }
                else if (config.caseValue == "title") {
                    var currVal = $(this).val();
                    $(this).val(currVal.charAt(0).toUpperCase() + currVal.slice(1).toLowerCase());
                }
                else if (config.caseValue == "pascal") {
                    var currVal = $(this).val();
                    currVal = currVal.toLowerCase().replace(/\b[a-z]/g, function (txtVal) {
                        return txtVal.toUpperCase();
                    });
                    $(this).val(currVal);
                }
            });
        });
    };
})(jQuery);

function searchPostCode(formid, postcode, house) {

    if (!jQuery('#' + house).val().trim() > '')
    {
        alert('Please enter house number or name');
        return false;
    }

    var term = jQuery('#' + postcode).val();
    if (!term.trim() > '') {
        alert('Please put a post code to search for.');
        return false;
    } else {

      
        var url = "https://www.sand-le-mere-bookings.co.uk/postcode/postcode.asp?PostCode=" + term;

        jQuery.get(url, function (data) {
            var xmlDoc = data;
            console.log(xmlDoc);
            $xml = jQuery(xmlDoc);
            console.log($xml);
            $address1 = $xml.find("AddressLine1");
            $address2 = $xml.find("AddressLine2");
            $address3 = $xml.find("AddressLine3");
            $postcode = $xml.find("PostCode");

            jQuery('#input' + formid + '_1').val(jQuery('#' + house).val() + " " + $address1.text());
            jQuery('#input' + formid + '_2').val($address2.text());
            jQuery('#input' + formid + '_3').val($address3.text());
            jQuery('#input' + formid + '_5').val($postcode.text());

        });


    }
}




