<?php
/* Template name: Accommodation Page */
get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h1><?php the_title(); ?></h1>
                    <div class = "content-text">
						<?php the_content(); ?>
					</div>
                </div>
            </div>
        </div>
        <div class="accommodation-listing">
			<?php
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				query_posts(
				array(
				  'post_type' => 'accommodation',
				  'posts_per_page' => -1,
				  'paged' => $paged )
			  );
			  if (have_posts()) {
				while (have_posts()) { the_post();
					$unitgrades = get_field('unit_grades');
					$image = get_field('unit_sub_image');
					
					$thumbnail_id = get_post_thumbnail_id(null);
					$feat_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
					if($feat_alt == "") {
						$feat_alt = get_the_title();
					}
			?>
					<div class="accom-wrapper">
						<div class="container">
							<div class="accom">
								<div class="price-flash">Short break from just <span>&pound;<?php the_field('unit_price_low') ?></span></div>
								<div class="row no-gutters">
									<div class="col-lg-6">
										<div class="img-wrapper">
											<img src="<?php echo get_the_post_thumbnail_url(null, 'accom-list') ?>" alt="<?= $feat_alt ?>">
											<?php if($image) {?>
												<div class="img-sm"><img src="<?php echo $image['sizes']['post-sidebar']; ?>" alt="<?php echo $image['alt']; ?>"></div>
											<?php } ?>	
										</div>
									</div>
									<div class="col-lg-6">
										<div class="content">
											<h3><?php the_title(); ?></h3>
											<h5><?php the_field('unit_sub_title') ?></h5>
											<?php if( in_array('pets', $unitgrades) ) { ?>
												<div class="pet-friendly"><i class="fas fa-paw"></i></div>
											<?php } ?>
											<span class="sleeps"><?php the_field('unit_size') ?></span>
											<?php if(get_field('unit_price_band_flash')) {?>
												<span class="price-band"><?php the_field('unit_price_band_flash') ?></span>
											<?php } ?>
											
											<?php if( have_rows('unit_features_list') ){
												echo '<ul>';
												$count = 0;
												while( have_rows('unit_features_list') ){ the_row(); 
											?>
													<li><?php the_sub_field('unit_features_list_content') ?></li>
											<?php 
													$count++;
													if($count == 6) break;
												};
												echo '</ul>';
											}; ?>
											
											<a href="<?php the_permalink() ?>" title="View <?php the_title(); ?>" class="btn outline violet">Take a look</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end accom -->
			<?php }
			 } wp_reset_query();
			 ?>
		</div>
	</section>
<?php get_footer(); ?>