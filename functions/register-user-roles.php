<?php

/** Add Owner role */
add_action('init', function () {
    add_role('owner', 'Owner', array(
        'manage_options' => false,
        'read' => false,
        'edit_posts' => false,
        'delete_posts' => false,
    ));
    $role = get_role('owner');
    $role->add_cap('owners_portal');
    $role->add_cap('edit_users');
});

?>