<?php
if( function_exists('acf_add_options_page') ) {

	// Theme options Tab
	acf_add_options_page(array(
		'page_title' => 'Theme Options',
		'menu_title' => 'Theme Options',
		'menu_slug' => 'theme-options',
		'icon_url' => 'dashicons-admin-settings', 
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Owners Portal',
		'menu_title'	=> 'Owners Portal',
		'parent_slug'	=> 'theme-options',
    ));
}
?>