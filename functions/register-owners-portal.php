<?php 

/** Check if current user is an owner */
add_action('init', function () {

    // If not logged in, return
    if(!is_user_logged_in()) {
        return;
    }

    // If is not in admin dashboard return
    if (!is_admin()) {
        return;
    }

    // Get current user
    $user = wp_get_current_user();

    // If is not an owner.
    // Add some admin script to hide unnecessary fields on add profile page
    if ($user->roles[0] != 'owner') {
        add_action('admin_enqueue_scripts', function () {
            echo '<style>
                #profile-page #your-profile h2,
                #profile-page #your-profile .user-admin-color-wrap,
                #profile-page #your-profile .show-admin-bar,
                #profile-page #your-profile .user-display-name-wrap,
                #profile-page #your-profile .user-description-wrap,
                #profile-page #your-profile .user-profile-picture,
                #profile-page #your-profile .user-url-wrap,
                #createuser table tbody tr:nth-child(5) {
                    display: none!important;
                }
            </style>';
        });
        return;
    }
    
    // User is logged in as an owner,
    // so include owner portal files
    require(TEMPLATEPATH . '/owners-portal/owners-portal.php');

});

?>