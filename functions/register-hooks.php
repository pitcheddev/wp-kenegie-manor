<?php 

/**
 * populates an ACF select field with gravity forms.
 */
add_filter('acf/load_field/name=theme_options_owners_portal_form_selection', function( $field ) {
	if (class_exists('GFFormsModel')) {
        $choices = [];
		foreach ( \GFFormsModel::get_forms() as $form ) {
			$choices[ $form->id ] = $form->title;
		}
		$field['choices'] = $choices;
	}
	return $field;
});


?>