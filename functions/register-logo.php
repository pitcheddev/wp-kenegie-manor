<?php 


add_action("login_enqueue_scripts", function () { ?>
    <style type="text/css">
        #login h1, 
        .login h1 {
            margin-bottom: 24px;
        }
        #login h1 a, 
        .login h1 a {
            width: 120px;
            height: 120px;
            background-image: url(<?php echo get_template_directory_uri(); ?>/img/kenegie-logo.png);
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
        }
    </style>
<?php });

?>