<?php 
/* Template name: No Sidebar Page */
get_header(); 
the_post(); 
?>

	<section class="content-wrapper">
        <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="w-100 text-center"><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>