jQuery(window).load(function () {

    if (jQuery('#HolidayTypeIDMob').val() > -1) {

        //console.log('init Datepicker index');

        //setDisabledDates(jQuery('#HolidayTypeID').val(), url);
        setDisabledDates(jQuery('#HolidayTypeIDMob').val(), url);
        
    }
    jQuery('#ArrivalDate').val("Select Date");

    jQuery('#Duration option:first').attr('selected', 'selected');

    jQuery('#ArrivalDateMob').val("Select Date");

    jQuery('#DurationMob option:first').attr('selected', 'selected');

});




jQuery('#SiteID').change(function () {


    //GetHolidayTypes($('#SiteID').val());
    //setDisabledDates(jQuery("#HolidayTypeID").val());
    setDisabledDates(1, 'https://bookings.kenegie-manor.co.uk/');

});


jQuery('#HolidayTypeID').change(function () {

    if (jQuery(this).val() > 0) {
        setDisabledDates(jQuery("#HolidayTypeID").val(), url);
    }

    jQuery('#ArrivalDate').val("Select Date");

    jQuery('#Duration option:first').attr('selected', 'selected');

    //hide touring dropdown if needed
    var thistype = jQuery("#HolidayTypeID option:selected").text();
    if (thistype.toLowerCase().trim() == "touring") {
        jQuery(".tourer-type").show();
    } else {
        jQuery(".tourer-type").hide();
    }

});

//jQuery('#HolidayTypeID').change();


jQuery('#ArrivalDate').change(function () {

    if (jQuery('#HolidayTypeID').val() > 0) {
       GetDuration(jQuery('#ArrivalDate').val(), jQuery('#HolidayTypeID').val(), url);
    }

});


jQuery('#HolidayTypeIDMob').change(function () {

    if (jQuery(this).val() > 0) {
        setDisabledDatesMob(jQuery("#HolidayTypeIDMob").val(), url);
    }

    jQuery('#ArrivalDateMob').val("Select Date");

    jQuery('#DurationMob option:first').attr('selected', 'selected');

    //hide touring dropdown if needed
    var thistype = jQuery("#HolidayTypeIDMob option:selected").text();
    if (thistype.toLowerCase().trim() == "touring") {
        jQuery(".tourer-type").show();
    } else {
        jQuery(".tourer-type").hide();
    }

});

jQuery('#ArrivalDateMob').change(function () {

    if (jQuery('#HolidayTypeIDMob').val() > 0) {
        GetDurationMob(jQuery('#ArrivalDateMob').val(), jQuery('#HolidayTypeIDMob').val(), url);
    }

});



function setDisabledDates(holidaytype, url) {

    jQuery("#ArrivalDateMob").datepicker("destroy");

    //Date Picker instantiation , including disabled dates
    //var array = ["06/11/2014", "09/11/2014", "12/11/2014"]
	
    jQuery.ajax({
        type: "GET",
        crossDomain: true,
        url: url + "/booking/GetDates/" + holidaytype,
        dataType: "json",
        async: false,
        success: function (response, status, xhr) {
            var array = [];
            //for (var key in response) {
            //    array.push(response[key]["fromdate"]);
            //}
			
            jQuery('#ArrivalDateMob').datepicker({

                beforeShowDay: function (date) {
					
                    var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
                    return [response.indexOf(string) == -1]
                },
                dateFormat: 'dd/mm/yy',
                minDate: new Date()
            }).datepicker();

        }
    });


}

//just place Duration nights that has available tariffs
function GetDuration(arrival, holidaytype, url) {

    jQuery.ajax({
        type: "GET",
        url: url + "/booking/GetDuration/" + holidaytype,
        dataType: "json",
        async: false,
        data: { ArrivalDate: arrival },
        success: function (response, status, xhr) {
            var markup = '';

            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i]) > 1)
                    markup += '<option value="' + response[i] + '">' + response[i] + ' nights</option>';
                else
                    markup += '<option value="' + response[i] + '">' + response[i] + ' night</option>';
            }

            jQuery("#Duration").html(markup);

        }
    });

}


function setDisabledDatesMob(holidaytype, url) {

    jQuery("#ArrivalDateMob").datepicker("destroy");

    //Date Picker instantiation , including disabled dates
    //var array = ["06/11/2014", "09/11/2014", "12/11/2014"]

    jQuery.ajax({
        type: "GET",
        crossDomain: true,
        url: url + "/booking/GetDates/" + holidaytype,
        dataType: "json",
        async: false,
        success: function (response, status, xhr) {
            var array = [];
            for (var key in response) {
                array.push(response[key]["fromdate"]);
            }

            jQuery('#ArrivalDateMob').datepicker({

                beforeShowDay: function (date) {
                    var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
                    return [array.indexOf(string) == -1]
                },
                dateFormat: 'dd/mm/yy',
                minDate: new Date()
            }).datepicker();

        }
    });


}

//just place Duration nights that has available tariffs
function GetDurationMob(arrival, holidaytype, url) {

    jQuery.ajax({
        type: "GET",
        url: url + "/booking/GetDuration/" + holidaytype,
        dataType: "json",
        async: false,
        data: { ArrivalDate: arrival },
        success: function (response, status, xhr) {
            var markup = '';

            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i]) > 1)
                    markup += '<option value="' + response[i] + '">' + response[i] + ' nights</option>';
                else
                    markup += '<option value="' + response[i] + '">' + response[i] + ' night</option>';
            }

            jQuery("#DurationMob").html(markup);

        }
    });

}
