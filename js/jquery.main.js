/*window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 90 || document.documentElement.scrollTop > 90) {
    document.getElementById("sticky-nav").style.top = "0";
  } else {
    document.getElementById("sticky-nav").style.top = "-90px";
  }
}*/

jQuery("#mmenu").mmenu({    
  }, {        
    offCanvas: {
      pageSelector: "#mmwrapper"
    }
  });
/*jQuery(".main-slider").slick({
    autoplay: true,
    arrows: false,
    dots: false,
    fade: true,
    cssEase: 'linear',
    infinite: true,
    speed: 1000,
    autoplaySpeed: 5000
});*/
jQuery(".main-slider").slick({
  autoplay: false,
  fade: true,
  speed: 600,
  infinite: true,
  dots: false,
  arrows: true,
  prevArrow: jQuery(".prev"),
  nextArrow: jQuery(".next")

});
jQuery(".client-portfolio-slider").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  dots: true,
  prevArrow: jQuery(".slider-prev"),
  nextArrow: jQuery(".slider-next"),
  centerMode: true,
  focusOnSelect: true,
  
  responsive: [{
    breakpoint: 480,
    settings: {
      slidesToShow: 1
    }
  }, {
    breakpoint: 767,
    settings: {
      slidesToShow: 2
    }
  }, {
    breakpoint: 1200,
    settings: {
      slidesToShow: 4
      
    }
  }]

});
if (jQuery(window).width() < 991) {
  jQuery('.offers-wrapper .row').attr('class', 'offers-slider');

  jQuery(".offers-slider").slick({
    dots: true,
    arrows: false,
    infinite: false
  });
}
/* Booking Widget */
if (jQuery(window).width() < 991) {
  jQuery('#booking-widget').addClass('collapse');
} else {
  jQuery('#booking-widget').removeClass('collapse');
}

jQuery('#btn-find-holiday').click(function() {
  var arrow = jQuery(this).find('i');
  console.log(arrow);
  if( arrow.hasClass('fal fa-angle-up') ) {
    arrow.removeClass('fal fa-angle-up').addClass('fal fa-angle-down');
  } else {
    arrow.removeClass('fal fa-angle-down').addClass('fal fa-angle-up');
  }
});

jQuery('#full-details').click(function() {
  var arrow = jQuery(this).find('i');
  console.log(arrow);
  if( arrow.hasClass('fal fa-angle-down') ) {
    arrow.removeClass('fal fa-angle-down').addClass('fal fa-angle-up');
  } else {
    arrow.removeClass('fal fa-angle-up').addClass('fal fa-angle-down');
  }
});

jQuery('.full-details').click(function() {
  if( jQuery(this).hasClass('close-details') ) {
    jQuery(this).removeClass('close-details').addClass('open-details');
  } else {
    jQuery(this).removeClass('open-details').addClass('close-details');
  }
});

jQuery('.card-header').click(function() {
  if( jQuery(this).hasClass('close-content') ) {
    jQuery(this).removeClass('close-content').addClass('open-content');
  } else {
    jQuery(this).removeClass('open-content').addClass('close-content');
  }
});

/*jQuery(".column-slider").slick({
    dots: true,
    infinite: false,
});*/
jQuery('.tabheader').click(function() {
  var arrow = jQuery(this).find('i');
  console.log(arrow);
  if( arrow.hasClass('fal fa-angle-up') ) {
    arrow.removeClass('fal fa-angle-up').addClass('fal fa-angle-down');
  } else {
    arrow.removeClass('fal fa-angle-down').addClass('fal fa-angle-up');
  }
});
jQuery('.accordionheader').click(function() {
  var arrow = jQuery(this).find('i');
  console.log(arrow);
  if( arrow.hasClass('fal fa-angle-up') ) {
    arrow.removeClass('fal fa-angle-up').addClass('fal fa-angle-down');
  } else {
    arrow.removeClass('fal fa-angle-down').addClass('fal fa-angle-up');
  }
});
jQuery('.accom-slider').each(function() {
  var holder = jQuery(this);
  var carousel = holder.find('.slideset');
  var btnPrev = holder.find('a.btn-prev');
  var btnNext = holder.find('a.btn-next');
  carousel.slick({
    slide: '.accom',
    infinite: false,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    adaptiveHeight: true,
    autoplaySpeed: 5000,
    accessibility: false,
    focusOnSelect: true,
    useCSS: true,
    draggable: false,
    mobileFirst: true,
    appendArrows: holder,
    prevArrow: '<a class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>',
    nextArrow: '<a class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>',
    responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 991,
      settings: {
        slidesToShow: 3
      }
    }]
  });
});
jQuery(".review-slider").slick({
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
});
jQuery(".ta-slider").slick({
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
});
jQuery(".back-to-top").click(function(e){
    return e.preventDefault(),jQuery("html, body").animate({scrollTop:0},500),!1});

jQuery("a.check-prices").click(function() {
    jQuery(".checkprices-overlay").fadeToggle(200);
    jQuery(this).toggleClass('btn-open').toggleClass('btn-close')
});
jQuery('.checkprices-overlay .btn-close').on('click', function() {
    jQuery(".checkprices-overlay").fadeToggle(200);
    jQuery(".check-prices").toggleClass('btn-open').toggleClass('btn-close');
    open=!1
});
jQuery('#tab_selector').on('change', function (e) {
  jQuery('#tabs li a').eq(jQuery(this).val()).tab('show');
});
jQuery(document).ready(function(){
    setDisabledDatesMob(1, url);
	
	//google tracking
    __gaTracker(function (tracker) {
		
       var linker = tracker.get('clientId');
		//console.log(linker);
	   jQuery('#clientId').val(linker);
	   
    });
	
});
var url = "https://kenegie.searchbreaks.com";
jQuery.ajaxSetup({ cache: false });



//setCookie('has-seen-modal', 'N');


/** 
 * Show Modal if enabled.
 * Also check if show once is enabled
 * and if so, if the user has already
 * seen the modal then don't display it.
 */
var pageModal = jQuery('#pageModal');
if (pageModal) {
    var hasSeen = getCookie('has-seen-modal') ? getCookie('has-seen-modal') : 'N';
    var displayFrequency = pageModal.data('display-frequency');
    if (displayFrequency == 0) 
        hasSeen = 'N';
    if (hasSeen == 'N') {
        setCookie('has-seen-modal', 'Y', displayFrequency); // Seen seen as Y for 30 days
        pageModal.appendTo("body");
        setTimeout(function() {
            pageModal.modal({
                'show' : true,
            });
        }, pageModal.data('opening-delay') * 1000);
    }
}

/**
 * Set a cookie, give it a unique id,
 * a value and set the number of days
 * before it's removed
 */
function setCookie(name, value, days) {
    var d = new Date();
    d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

/**
 * Get a cookie by its unique ID
 */
function getCookie(name) {
    var name = name + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}



