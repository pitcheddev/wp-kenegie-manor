	<div class="main-slider-wrapper">
		<?php if( have_rows('home_slides') ){ ?>
			<div class="main-slider">
				<?php if( have_rows('home_slides') ){ ?>
				<?php while( have_rows('home_slides') ){ the_row();
					// vars
					$slide_img = get_sub_field('slide');
					$alt = $image_sub['alt'];
					$hasText = false;
					$position = "right";
					if(get_sub_field('label_position') && get_sub_field('label_position') == "Left"){
						$position = "left";
					}
				?>
				<div>    
					<div class="text-wrapper" >
						<?php if(get_sub_field('description') || get_sub_field('link')) { $hasText = true; ?>
						<div class="text <?= $position ?>">
							<?php if(get_sub_field('description')){ ?>
								<h2><?php the_sub_field('description') ?></h2>
							<?php } ?>
							
							<?php if(get_sub_field('link')){
								$label = "Find out more";
								if(get_sub_field('label_link')) $label = get_sub_field('label_link');
								?>
								<a href="<?php the_sub_field('link') ?>" class="btn solid light-violet shadow"><?= $label ?></a>
							<?php } ?>
							
						</div>
						<?php } ?>
					</div>   
					<?php if($hasText){ ?>
					<picture>
						<source srcset="<?php echo $slide_img['sizes']['home-slider-sm']; ?>" media="(max-width: 480px)">
						<source srcset="<?php echo $slide_img['sizes']['home-slider-md']; ?>" media="(max-width: 768px)">
						<source srcset="<?php echo $slide_img['sizes']['home-slider-lg']; ?>">
						<img src="<?php echo $slide_img['sizes']['home-slider-lg']; ?>" alt = "<?= $alt ?>">
					</picture>  
					<?php } else{ ?>
					<picture>
						<source srcset="<?php echo $slide_img['sizes']['home-slider-xs']; ?>" media="(max-width: 480px)">
						<source srcset="<?php echo $slide_img['sizes']['home-slider-sm_2']; ?>" media="(max-width: 768px)">
						<source srcset="<?php echo $slide_img['sizes']['home-slider-md_2']; ?>" media="(max-width: 991px)">
						<source srcset="<?php echo $slide_img['sizes']['home-slider-lg']; ?>">
						<img src="<?php echo $slide_img['sizes']['home-slider-lg']; ?>" alt = "<?= $alt ?>">
					</picture> 
					<?php }  ?>					
				</div><!-- end slide -->
				<?php }
				}
				?>
			</div><!-- end main slider -->
			<div class="prev-next">
				<a href="#" class="prev"><i class="fal fa-angle-left"></i></a>
				<a href="#" class="next"><i class="fal fa-angle-right"></i></a>
			</div>
		<?php 
		}else{?>
		 
			<div class="main-slider">
				<div>         
					<picture>
						<source srcset="<?php echo get_the_post_thumbnail_url(null, 'home-slider-sm'); ?>" media="(max-width: 480px)">
						<source srcset="<?php echo get_the_post_thumbnail_url(null, 'home-slider-md'); ?>" media="(max-width: 768px)">
						<source srcset="<?php echo get_the_post_thumbnail_url(null, 'home-slider-lg'); ?>">
						<img src="<?php echo get_the_post_thumbnail_url(null, 'home-slider-lg'); ?>" alt = "<?= $alt ?>">
					</picture>            
				</div>
			</div><!-- end main slider -->
			
		<?php }
		?>

	</div>