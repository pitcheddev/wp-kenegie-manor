<?php 

class PageNewsletter {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'owners_portal_newsletter')) { 
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_menu_page('Newsletter', 'Newsletter', 'owners_portal', 'owners_portal_newsletter', array($this, 'render'), 'dashicons-admin-post', 2);
        });
    }

    public function render () {
        include(TEMPLATEPATH . '/owners-portal/template-newsletter.php');
    }
}