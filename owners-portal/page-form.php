<?php 

class PageForm {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_menu_page("Owner's Booking Form", "Owner's Booking Form", 'owners_portal', 'owners_portal_form', array($this, 'render'), 'dashicons-email', 5);
        });
    }

    public function render () {
        include(TEMPLATEPATH . '/owners-portal/template-form.php');
    }
}