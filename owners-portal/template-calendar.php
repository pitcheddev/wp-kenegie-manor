<?php 
    // Set api request timeout
    add_filter('http_request_timeout', function($time) {
        return 60;
    });

    // Get owner details
    $owner = wp_get_current_user();
    $owner_id = $owner->ID;
    $owner_name = $owner->display_name;

    // Get unit number from url
    $unit_number = end(explode('_', $_GET['page']));
    $unit_name = "";
    $unit_ical_url = "";

    // GMatch this unit against the properties repeater for this unit
    // to retrieve relative data such as unit name and ical url.
    $properties = get_field('profile_owner_properties', 'user_' . $owner_id);
    if ($properties) { 
        foreach ($properties as $property) {
            if ($property['unit_number'] == $unit_number) {
                $unit_name = $property['unit_name'];
                $unit_ical_url = $property['ical_url'];
                break;
            }
        }
    }

    // Get data from theme options
    $calendar_intro_text = get_field('theme_options_owners_portal_calendar_intro_text', 'options');
    $calendar_alert_box_enabled = get_field('theme_options_owners_portal_calendar_alert_box_enabled', 'options');

    // Make POST request to get ical
    $request = wp_remote_get($unit_ical_url);

    // If there was an error, set the error message.
    // Else continue with function
    if(is_wp_error($request)) {
        $ical_contents = null;
    } else {
        $ical_contents = wp_remote_retrieve_body($request);
    }

?>
<div class="owners-portal-page p-5" id="owners-portal-page-calendar">
    <textarea id="ical-contents" hidden><?php echo $ical_contents ?></textarea>
    <div class="container p-0">
        <div class="row">
            <div class="col-9 align-self-center">
                <h2 class="mt-0 mb-3">Hi, <?php echo $owner_name; ?>.</h2>
                <h1 class="mt-0 mb-3"><?php echo $unit_number; ?>, <?php echo $unit_name; ?></h1>
                <p class="mt-0 mb-0"><?php echo $calendar_intro_text; ?></p>
                <?php 
                    if ($calendar_alert_box_enabled) {
                        $calendar_alert_box_text = get_field('theme_options_owners_portal_calendar_alert_box_text', 'options');
                        $calendar_alert_box_button_enabled = get_field('theme_options_owners_portal_calendar_alert_box_button_enabled', 'options');
                        $calendar_alert_box_button_label = get_field('theme_options_owners_portal_calendar_alert_box_button_label', 'options');
                        $calendar_alert_box_button_url = get_field('theme_options_owners_portal_calendar_alert_box_button_url', 'options');
                        ?>
                        <div class="acf-admin-notice notice notice-error mx-0 mb-0 mt-3">
                            <p class="mt-0 mb-0">
                                <?php echo $calendar_alert_box_text; ?>
                            </p>
                            <?php if ($calendar_alert_box_button_enabled) { ?> 
                                <a class="button button-secondary mt-1 mb-1" href="<?php echo $calendar_alert_box_button_url; ?>" target="_blank"><?php echo $calendar_alert_box_button_label; ?></a>
                            <?php } ?>
                        </div>
                    <?php }
                ?>
            </div>
            <div class="col-3 text-right align-self-center pl-5">
                <img class="position-relative d-block w-100 h-auto" style="max-width:210px" src="<?php echo get_template_directory_uri() ?>/img/kenegie-cat.png" />
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="owners-portal-page-calendar-wrapper">
                    <?php if ($ical_contents) { ?> 
                        <div class="owners-portal-page-filter p-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="d-inline-block m-0">Filter By Arrival Date</h4>
                                    <input class="d-inline-block ml-2" type="text" id="datepicker-from-date" placeholder="From Date">
                                    <input class="d-inline-block ml-2" type="text" id="datepicker-to-date"  placeholder="To Date">
                                    <button class="button button-secondary d-inline-block ml-2" id="calendar-filter-btn">Filter</button>
                                    <button class="button button-secondary d-inline-block ml-2" id="calendar-filter-clear-btn">Clear</button>
                                </div>
                            </div>
                        </div>
                        <div class="owners-portal-page-calendar-table"></div>
                    <?php } else { ?>
                        <h3 class="my-4 mx-0 text-center">There are currently no bookings for this property.</h3>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>