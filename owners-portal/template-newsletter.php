<div class="owners-portal-page p-5" id="owners-portal-page-newsletter">
    <div class="container p-0">
        <div class="row pb-5" style="border-bottom: 1px solid #e1e1e1">
            <div class="col-9 align-self-center">
                <h1 class="mt-0 mb-3">Owners Newsletter</h1>
                <p class="mt-0 mb-0"><?php echo get_field('theme_options_owners_portal_newsletter_intro_text', 'options'); ?></p>
            </div>
            <div class="col-3 text-right align-self-center pl-5">
                <img class="position-relative d-block w-100 h-auto" style="max-width:210px" src="<?php echo get_template_directory_uri() ?>/img/kenegie-cat-newsletter.png" />
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <?php 
                $query = new WP_Query(array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'posts_per_page' => -99,
                    'category_name' => "owners",
                )); 
                if ($query->have_posts()) {
                    while ($query->have_posts()) { $query->the_post(); ?>
                        <div class="col-12 col-sm-10 col-md-8">
                            <div class="owners-portal-news-item p-4 mb-5">	
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'post-img') ?>" class="attachment-post-img size-post-img wp-post-image" loading="lazy">										
                                    </div>
                                    <div class="col-md-8 mt-3 mt-md-0">
                                        <div class="content">
                                            <h2 class="mt-2 mb-3"><?php echo get_the_title(get_the_ID()) ?></h2>
                                            <p class="mt-0 mb-3"><?php excerpt(35); ?></p>
                                            <a href="<?php echo get_the_permalink(get_the_ID()); ?>" target="_blank" class="button button-primary">READ THIS ARTICLE</a>
                                        </div>
                                    </div>
                                </div>                        
                            </div>                        
                        </div>
                    <?php }
                } wp_reset_query();
            ?>
        </div>
    </div>
</div>