<?php 

class PageCalendar {

    public function __construct() {
        $this->owner = wp_get_current_user();
        $this->properties = get_field('profile_owner_properties', 'user_' . $this->owner->ID);
        $this->registerScripts();
        $this->createMenu();

        
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'owners_portal_calendar')) { 

                 /*wp_enqueue_style('hendra-tariff-css', plugin_dir_url( __DIR__ ) . '../css/style.min.css');
                wp_enqueue_script('hendra-tariff-notification', plugin_dir_url(__DIR__) . '../js/plugins/notification.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-tablefy', plugin_dir_url(__DIR__) . '../js/plugins/tablefy.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-form-modal', plugin_dir_url(__DIR__) . '../js/plugins/form-modal.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-glamping-pods-dao', plugin_dir_url(__DIR__) . '../js/daos/glamping-pods-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-glamping-pods-page-template', plugin_dir_url(__DIR__) . '../js/pages/glamping-pods-page-template.js', array('jquery', 'hendra-tariff-form-modal', 'hendra-tariff-tablefy'), 1.0, true);*/
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){

            // Create menu page
            //add_menu_page('Calendar', 'Calendar', 'owners_portal', 'owners_portal_calendar', array($this, 'render'), 'dashicons-calendar', 1);
            //add_submenu_page('owners_portal_calendar', 'Test', 'test', 'owners_portal', 'owners_portal_calendar?id=1', array($this, 'render'));
            // Create sub menu page for each property
            if ($this->properties) {
                foreach ($this->properties as $property) {
                    add_menu_page($property['unit_name'], $property['unit_name'], 'owners_portal', 'owners_portal_calendar_' . $property['unit_number'], array($this, 'render'), 'dashicons-calendar', 1);
                }

            }
        });
    }

    public function render () {
        include(TEMPLATEPATH . '/owners-portal/template-calendar.php');
    }
}