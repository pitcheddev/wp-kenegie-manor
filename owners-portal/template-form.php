<?php 
    // Get owner details
    $owner = wp_get_current_user();
    $owner_id = $owner->ID;
    $owner_name = $owner->display_name;
    $owner_first_name = $owner->user_firstname;
    $owner_last_name = $owner->user_lastname;
    $owner_email = $owner->user_email;
?>

<div class="owners-portal-page p-5" id="owners-portal-page-newsletter">
    <div class="container p-0">
        <div class="row pb-5" style="border-bottom: 1px solid #e1e1e1">
            <div class="col-9 align-self-center">
                <h1 class="mt-0 mb-3">Owner's Booking Form</h1>
                <p class="mt-0 mb-0"><?php echo get_field('theme_options_owners_portal_form_intro_text', 'options'); ?></p>
            </div>
            <div class="col-3 text-right align-self-center pl-5">
                <img class="position-relative d-block w-100 h-auto" style="max-width:210px" src="<?php echo get_template_directory_uri() ?>/img/kenegie-cat-form.png" />
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-12">
                <?php gravity_form(
                    get_field('theme_options_owners_portal_form_selection', 'options'), 
                    false, 
                    false, 
                    false, 
                    array(
                        'email' => $owner_email, 
                        'first_name' => $owner_first_name,
                        'last_name' => $owner_last_name,
                    ), 
                    true, 
                    500, 
                    true
                ); ?>
            </div>
        </div>
    </div>
</div>