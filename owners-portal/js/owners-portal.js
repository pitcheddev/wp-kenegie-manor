/**
 * Calendar page
 */
(function($) {

    // Get owners page
    var $ownersPage = $('#owners-portal-page-calendar');

    // If not on calendar page
    if ($ownersPage.length == 0) 
        return;

    // Get ical contents
    var iCalContent = $ownersPage.find('#ical-contents').val();
    
    // If ical URL wasn't provided, we need to show an error
    if (!iCalContent) {
        return alert("Sorry, we were unable to retrieve your calendar.");
    }

    // Get events from ical content
    var events = parseICal(iCalContent);
    console.log(events);

    // Reference some elements
    var $calendarTable = $('.owners-portal-page-calendar-table');
    var $datepickerFromDate = $("#datepicker-from-date");
    var $datepickerToDate = $("#datepicker-to-date");
    var $filterBtn = $('#calendar-filter-btn');
    var $filterClearBtn = $('#calendar-filter-clear-btn');

    // Init tablefy
    $calendarTable.tablefy({
        'columnModel': [
            { label: "uid", field_name: "uid", hidden: true, },
            { label: "Surname", field_name: "surname", hidden: false, },
            { label: "Arrival Date", field_name: "arrivalDate" },
            { label: "Departure Date", field_name: "departureDate" },
            { label: "Duration", field_name: "duration" },
            { label: "Status", field_name: "status" },
        ],
    });

    // Init datepickers
    $datepickerFromDate.datepicker({
        dateFormat: "dd/mm/yy",
        minDate: -30,
    });
    $datepickerToDate.datepicker({
        dateFormat: "dd/mm/yy",
        minDate: 0,
    });

    // CLicking filter btn
    $filterBtn.on('click', function () {
        renderTable();
    });

    // CLicking clear filter btn
    $filterClearBtn.on('click', function () {
        $datepickerFromDate.datepicker("setDate", null);
        $datepickerToDate.datepicker("setDate", null);
        renderTable();
    });


    // Render table
    renderTable();


    /**
     * Render table
     */
    function renderTable() {
        var filteredEvents = events.filter((event) => {
            var fromDate = $datepickerFromDate.datepicker("getDate");
            var toDate = $datepickerToDate.datepicker("getDate");
            if (fromDate && toDate) {
                if (event.startDate >= fromDate && event.startDate <= toDate) {
                    return true;
                }
                return false;
            } else {
                return true;
            }
        }).map((event) => {
            var status = '';
            var currentDate = new Date();
            if (event.endDate < currentDate) {
                status = 'departing';
            } else if (event.startDate > currentDate) {
                status = 'upcoming';
            } else if (event.startDate < currentDate && event.endDate > currentDate) {
                status = 'ongoing';
            }
            return {
                uid: event.uid,
                surname: event.surname ? event.surname : '#####',
                arrivalDate: event.startDate.toLocaleDateString('en-GB'),
                departureDate: event.endDate.toLocaleDateString('en-GB'),
                duration: Math.round(Math.abs((event.startDate - event.endDate) / (24 * 60 * 60 * 1000))) + ' nights',
                status: '<span class="status-span status-span-' + status + '">' + status + '</span>'
            }
        });
        if (filteredEvents.length == 0) {
            $calendarTable.tablefy('rowModel', [{ surname: '-', arrivalDate: '-', departureDate: '-', duration: '-', status: '-' }]);
        } else {
            $calendarTable.tablefy('rowModel', filteredEvents);
        }

    }

    /**
     * Parse ical
     * @param {*} iCalString 
     * @returns 
     */
    function parseICal(iCalString) {

        // Split iCal line by line
        var iCalContentLines = iCalString.split(/\r\n|\n/);

        // Create an array for events
        var icalEvents = [];

        // Has event building started?
        var eventHasStarted = false;

        // Reference for current event that is being build
        var currentEvent = {};

        // Read iCal line by line to strip out events
        iCalContentLines.forEach((line) => {

            // An event has started
            if (line === 'BEGIN:VEVENT') {
                eventHasStarted = true;
            } 
            
            // The event has now ended,
            // So add the event to the icalEvents array.
            // Also reset the currentEvent object.
            else if (line === 'END:VEVENT') {
                eventHasStarted = false;
                icalEvents.push(currentEvent);
                currentEvent = {};
            } 
            
            // Event content has started, so 
            // parse each line between the BEGIN and END event tags
            else if (eventHasStarted) {
                var lineSplit = line.split(":");
                var attributeName = lineSplit[0];
                var attributeValue = lineSplit[1];
                if (attributeName == 'UID') {
                    currentEvent.uid = attributeValue;
                } else if (attributeName == 'DESCRIPTION') {
                    currentEvent.description = attributeValue;
                } else if (attributeName == 'SEQUENCE') {
                    currentEvent.sequence = attributeValue;
                } else if (attributeName == 'SUMMARY') {
                    currentEvent.surname = attributeValue.replace(" (Pitched)", "");
                } else if (attributeName == 'DTSTART;VALUE=DATE') {
                    currentEvent.startDate = parseICalDate(attributeValue);
                } else if (attributeName == 'DTEND;VALUE=DATE') {
                    currentEvent.endDate = parseICalDate(attributeValue);
                } else if (attributeName == 'DTSTAMP') {
                    currentEvent.dateStamp = parseICalDate(attributeValue);
                }
            }

        });

        /**
         * Parse an ical datestring like yyymmdd into a Date object.
         * @param {*} dateString 
         * @returns 
         */
        function parseICalDate(dateString) {
            var year = dateString.substring(0,4);
            var month = dateString.substring(4,6);
            var day = dateString.substring(6,8);
            var date = new Date(year, month-1, day);
            return date;
        }

        // Return events
        return icalEvents;
    }


})(jQuery);

/**
 * Profile page
 */
(function($) {

    // If not on profile page, return
    if ($('#profile-page').length == 0)
        return;


    // Set some fields to read only
    $('[data-name="profile_owner_ical_url"] input').attr('readonly', true).css('cursor', 'not-allowed');
    $('[data-name="profile_owner_unit_number"] input').attr('readonly', true).css('cursor', 'not-allowed');
    $('[data-name="profile_owner_unit_name"] input').attr('readonly', true).css('cursor', 'not-allowed');

})(jQuery);

/**
 * Other 
 */
(function($) {

    // Manually change the community url. Since WP does not allow
    // menu items to link to external urls.
    var link = $('#toplevel_page_owners_portal_community a');
    link.attr('href', 'https://kenegiemanor.discussion.community/');
    link.attr('target', '_blank')

})(jQuery);