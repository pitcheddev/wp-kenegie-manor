<?php 

class PageCommunity {

    public function __construct() {
        $this->owner = wp_get_current_user();
        $this->createMenu();
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_menu_page('Community', 'Community', 'owners_portal', 'owners_portal_community', '', 'dashicons-groups', 3);
        });
    }

}