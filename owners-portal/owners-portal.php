<?php 

// Require calendar page
require(TEMPLATEPATH . '/owners-portal/page-calendar.php');
require(TEMPLATEPATH . '/owners-portal/page-newsletter.php');
require(TEMPLATEPATH . '/owners-portal/page-community.php');
require(TEMPLATEPATH . '/owners-portal/page-form.php');

// Enqueue scripts
add_action('admin_enqueue_scripts', function () {
    wp_enqueue_script('font-typekit', "https://use.typekit.net/qii0bin.js");
    wp_add_inline_script( 'font-typekit', 'try{Typekit.load({ async: true });}catch(e){}');
    wp_enqueue_style('owners-portal', get_template_directory_uri() . '/owners-portal/css/owners-portal.min.css');
    wp_enqueue_style('jquery-ui-datepicker-style', 'https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('tablefy', get_template_directory_uri() .'/owners-portal/js/tablefy.js', array('jquery'), 1.0, true);
    wp_enqueue_script('owners-portal', get_template_directory_uri() .'/owners-portal/js/owners-portal.js', array('jquery'), 1.0, true);
});


// Create calendar page
new PageCalendar();
new PageNewsletter();
new PageCommunity();
new PageForm();




?>