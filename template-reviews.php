<?php
/* Template name: Reviews Page */
get_header(); the_post(); ?>

	<section class="content-wrapper">
        <div class="container">
			<div class="row">
				<div class="col-lg-9">
					<h1><?php the_title(); ?></h1>
					<div class = "content-text">
						<?php the_content(); ?>
					</div>
					
					<div class="offers-page-wrapper">
					<?php
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						query_posts(
						array(
						  'post_type' => 'review',
						  'posts_per_page' => -1,
						  'orderBy' => 'rand',
						  'paged' => $paged )
						);
						if (have_posts()) : 
							while (have_posts()) : the_post();
					?>
								<!-- Review loop -->
								<div class="offer">
									<div class="row">
										<div class="col-sm-12">
											<div class="content">
												<?php the_content(); ?>
											</div>
										</div>
									</div>
								</div>

					<?php endwhile;
						  endif;
					wp_reset_query();
					?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>