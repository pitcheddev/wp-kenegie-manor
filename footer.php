		<section class="cta">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h2>Ready to book?</h2>
						Call the holiday hotline free on
						<span class="phone">0800 043 6449</span>
					</div>
					<div class="col-10 offset-1 col-lg-6 offset-lg-0">
						<div class="sign-up">
							<span>Subscribe for amazing holiday offers by email</span>
							<?php echo do_shortcode('[gravityform id="3" title="false" description="false" tab-index="100"]'); ?>
							<p>By submitting this form you are agreeing to receive marketing communications from Kenegie Manor. To find out more about our privacy policy and what information we will send you <a href="/data-protection/">click here</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<footer id="footer">        
			<div class="container">            
				<div class="row">
					<div class="vector"></div>
						<div class="col-md-4">
							<h3>Kenegie Manor Holiday Park</h3>
							<span class="address">Gulval, Penzance, Cornwall, TR20 8YN</span>
							<a href="/cornwall-hoiday-park/contact-us/" class="btn solid light-violet">Ask a question?</a>
						</div>
						<div class="col-md-8">
						<nav class="footer-nav">
							<?php kenegie_quick_links_menu(); ?>
						</nav>
						<nav class="footer-nav">
							<?php kenegie_important_information(); ?>
						</nav>
						<div class="award">
							<a href="https://www.tripadvisor.co.uk/Hotel_Review-g1755115-d1935620-Reviews-Kenegie_Manor_Holiday_Park-Gulval_Penzance_Cornwall_England.html"><img src="<?php echo get_template_directory_uri(); ?>/images/TC.png" alt="Kenegie Manor Tripadvisor" height="200" width="180"></a>
						</div>
					</div>
					<div class="col-12">
						<p>Kenegie Manor Holiday park is owned and operated by McKinnon Jardine Leisure Ltd<br>
						Registered Office: The Estate Offices, Kenegie Manor, Gulval, Penzance Cornwall TR20 8YN  |  Company No: 09637325  |  VAT: 221 700362</p>
						<div class="websiteby">
							<span>Website by</span>
							<a href="https://www.pitched.co.uk"><img src="<?php echo get_template_directory_uri(); ?>/images/pitched-logo.png" alt="Pitched"/></a>
							Copyright Kenegie Manor Holiday Park <?php echo date('Y'); ?>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>
<nav id="mmenu">
    <?php kenegie_get_header_menu(); ?>
</nav>
<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/@pitched/pitched-widget-standard@latest/dist/pitched-widget-standard.min.js"></script>
<script>
	var widgetDesktop = new PitchedWidget({
		'bookingURL': 'https://api.pitchedbooking.com/booking/614c5687-3e03-4485-8c48-8adc708873b8',
		'key': 'cd183e7cd86a48b2a7f0f23a7f6164b1',
		'appendTo': '#BookingWidgetDesktop',
		'color': '#fbc132',
		'buttonLabel': 'SEARCH',
		'collapsed': false
	});
</script>
</body>
</html>