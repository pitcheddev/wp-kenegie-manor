	<section class="booking-widget">
	    <?php wp_enqueue_script('pitchedbooking'); ?>
        <div class="container">
			<div class="row">
				<div class="col-sm-12">
					<a href="#" id="btn-find-holiday" data-toggle="collapse" data-target="#booking-widget">Find a holiday <i class="fal fa-angle-down"></i></a>
					<div class="form-wrapper collapse text-center" id="booking-widget">
						<h3 style="width:100%">Find your perfect break away...</h3>
						<div id="BookingWidgetDesktop"></div>
						<!--
						<form action="https://kenegie.searchbreaks.com/Booking/CheckAvailabilty" method="post">
						    <input type="hidden" id="HolidayTypeIDMob" name="HolidayTypeID" value="1">
							<input type="hidden" id="SiteID" name="SiteID" value="1">
							<div>
								<label>Arrival date</label>
								<div>
								  <input id="ArrivalDateMob" name="ArrivalDate" readonly="true" type="text" />
								</div>
							</div>
							<div>
								<label>Duration</label>
								<div class="custom-selectbox">
								  <select data-val="true" data-val-number="The field Duration must be a number." data-val-required="The Duration field is required." id="DurationMob" name="Duration">
									<option selected="selected" value="">Duration</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
								  </select>
								</div>
							</div>
							<div>
								<label>Adults (18+)</label>
								<div class="custom-selectbox">
								  <select data-val="true" data-val-number="The field Adults must be a number." data-val-required="The Adults field is required." id="AdultsMob" name="Adults">
									<option selected="selected" value="">Adults</option>
                            		<option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
								  </select>
								</div>
							</div>
							<div>
								<label>Children (under 17)</label>
								<div class="custom-selectbox">
								  <select data-val="true" data-val-number="The field Children must be a number." data-val-required="The Children field is required." id="ChildrenMob" name="Children">
									<option selected="selected" value="0">Children</option>
                                    <option>0</option>
                            		<option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
								  </select>
								</div>
							</div>
							<div>
								<label>Pets</label>
								<div class="custom-selectbox">
								  <select data-val="true" data-val-number="The field Pets must be a number." data-val-required="The Pets field is required." id="Pets" name="Pets">
								    <option selected="selected" value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                  </select>
								</div>
							</div>
							<input type="submit" value="Search" class="btn solid yellow">
						</form>-->
					</div>
				</div>
			</div>
        </div>
	
    </section>