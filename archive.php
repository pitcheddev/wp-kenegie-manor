<?php get_header(); ?>
<section class="inner-page">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-9 col-lg-9">
          <div class="content-wrapper posts-page">
            <h1>
            <?php
              if ( is_day() ) :
                printf( __( 'News Archive: %s' ), get_the_date() );

              elseif ( is_month() ) :
                printf( __( 'News Archive: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );

              elseif ( is_year() ) :
                printf( __( 'News Archive: %s' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

              else :
                _e( 'Kenegie Manor news, events and holiday tips!' );

              endif;
            ?>
				    </h1>
            <hr />
            <?php
                  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                  query_posts(
                    array(
                      'posts_per_page' => 12,
                      'paged' => $paged )
                  );
                  if (have_posts()) :
                  while (have_posts()) :
                  the_post();
                ?>
              <!-- Post loop -->
                  <div class="accommodation">
                    <div class="row">
                      <div class="col-sm-4">
                         <?php the_post_thumbnail('post-img'); ?>
                      </div>
                      <div class="col-sm-8">
                        <div class="details">
                          <h2><?php the_title(); ?></h2>
                          <p><?php excerpt(35); ?></p>
                          <a href="<?php the_permalink(); ?>" class="btn-violet">Read this article</a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php endwhile;
                      endif;
                wp_reset_query();
                ?>
          </div>
        </div>
        <?php get_sidebar(); ?>
      </div>
    </div>
  </section>
<?php get_footer(); ?>