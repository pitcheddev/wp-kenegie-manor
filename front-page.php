<?php get_header(); the_post();
$image_flash = get_field('accommodation_flash_image'); ?>
	<section class="intro">
        <div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h2><?php the_field('has_top_label'); ?></h2>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					
					<a href="<?php the_field('has_about_our_park_link'); ?>" class="btn outline violet">About our park</a>
					<a href="<?php the_field('has_our_facilities_link'); ?>" class="btn outline violet">Our facilities</a>
				</div>
				<div class="col-lg-6">
					<div class="img">
						<img src="<?php echo $image_flash['sizes']['accom-list']; ?>" alt="<?php echo $image_flash['alt']; ?>">
						<div class="price-flash">
							Short breaks from just <span>&pound;<?php the_field('flash_accom'); ?></span> for the whole family!
						</div>
					</div>
				</div>
			</div>
        </div>
    </section>
	
	<section class="park-highlights">
        <div class="container">
			<div class="row">
				<?php if( have_rows('has_highlight_list') ): 
					while( have_rows('has_highlight_list') ): the_row(); 
						$icon = "";
						if(get_sub_field('has_highlight_icontype') == 'font_awesome'){
							$icon = '<i class="fas '.get_sub_field('has_highlight_fa_icon').'"></i>';
						}
				?>
					<div class="col-6 col-lg-3">
						<div class="highlight">
							<?= $icon ?>
							<?php the_sub_field('has_highlight_content') ?>
						</div>
					</div>
				<?php endwhile;
				endif; ?>
			</div>
        </div>
    </section>

    <section class="accommodation">
        <div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 text-center">
					<h2><?php the_field('title_accom'); ?></h2>
					<?php the_field('content_accom'); ?>
				</div>
			</div>
			<div class="accom-slider">
				<div class="mask-holder">
					<div class="mask">
						<div class="slideset" data-slick='{"autoplay": false}'>
							<?php if( have_rows('unit_list') ): 
								while( have_rows('unit_list') ): the_row(); 
								$image = get_sub_field('unit_image');
							?>
								<article class="accom">
									<img src="<?php echo $image['sizes']['home-accom']; ?>" alt="<?php echo $image['alt']; ?>">
									<h3><?php the_sub_field('unit_title') ?></h3>
									<span><?php the_sub_field('unit_sub_title') ?></span>
									<?php the_sub_field('unit_description') ?>
									<?php if(get_sub_field('unit_link')){
										$label = get_sub_field('unit_link_label') ? get_sub_field('unit_link_label') : 'View The Range'; ?>
										<a href="<?php the_sub_field('unit_link') ?>" class="btn outline violet"><?= $label ?></a>
									<?php } ?>
								</article>
							<?php endwhile;
							endif; ?>
							
							<?php if( have_rows('home_features') ): 
								while( have_rows('home_features') ): the_row(); 
								$image = get_sub_field('feat_image');
							?>
								<article class="accom">
									<img src="<?php echo $image['sizes']['home-accom']; ?>" alt="<?php echo $image['alt']; ?>">
									<h3><?php the_sub_field('feat_title') ?></h3>
									<span><?php the_sub_field('feat_sub_title') ?></span>
									<?php the_sub_field('feat_description') ?>
									<?php if(get_sub_field('feat_link')){
										$label = get_sub_field('feat_button') ? get_sub_field('feat_button') : 'View The Range'; ?>
										<a href="<?php the_sub_field('feat_link') ?>" class="btn outline violet"><?= $label ?></a>
									<?php } ?>
									
								</article>
							<?php endwhile;
							endif; ?>
						</div>
					</div>
				</div>
			</div>
        </div>
    </section>
	<?php
	$image_right = get_field('hdc_right_image');
	$image_left = get_field('hdc_left_image');
	?> 
	<section class="discover-cornwall">
        <div class="image">
			<img src="<?php echo get_template_directory_uri(); ?>/images/banners/cornwall.jpg" alt="Cornwall">
		</div>
		<div class="content-wrapper">
			<div class="vector">
				<img src="<?php echo get_template_directory_uri(); ?>/images/others/vector.png" alt="Cornwall">
			</div>
			<div class="text-wrapper">
				<h2><?php the_field('hdc_title') ?></h2>
				<p><?php the_field('hdc_content') ?></p>
				<a href="<?php the_field('hdc_link') ?>" class="btn outline white">Discover Cornwall</a>
			</div>
		</div>
		<div class="gal-img1"><img src="<?php echo $image_left['sizes']['home-accom']; ?>" alt="<?php echo $image_left['alt']; ?>"></div>
		<div class="gal-img2"><img src="<?php echo $image_right['sizes']['accom-list']; ?>" alt="<?php echo $image_right['alt']; ?>"></div>
    </section>  
	
	<section class="offers">
        <div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 text-center">
					<h2>Are you ready for an adventure?</h2>
					<h4>Holiday deals not to be missed...</h4>
					<?php the_field('has_offer_content'); ?>
					<a href="/special-offers/" class="btn outline violet">View all offers</a>
				</div>
			</div>
			<div class="offers-wrapper">
				<?php 
				$flash = get_field('show_offer_flashes');
				if($flash) {
					?>
					<div class="flash"></div>
					<div class="asbo"></div>
				<?php } ?>
				<div class="row d-flex justify-content-center">
				<?php
				//get offers
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					query_posts(
					  array(
						'post_type' => 'special_offer',
						'posts_per_page' => 4,
						'orderby' => 'rand',
						'paged' => $paged )
					);
					if (have_posts()) :
						while (have_posts()) :
							the_post();
							$image = get_field('offer_image');
				?>
						<div class="col-6 col-lg-3 mb-3">
							<div class="offer">
								<img src="<?php echo $image['sizes']['home-feature']; ?>" alt="<?php echo $image['alt']; ?>">
								<div class="content">
									<h3><?php the_title(); ?></h3>
									<?php the_field('offer_sub_heading'); ?>
								</div>
								<a href="/special-offers/" class="btn outline violet">View Details</a>
							</div>
						</div>
				<?php endwhile;
                endif;
				wp_reset_query();
				?>
				
				</div>
			</div>
        </div>
      </section>



<?php if (get_field('modal_enabled') == true){ 
    if (get_field('modal_type') == 'promotion') {
        get_template_part( 'partials/promotion', 'modal' );
    } else if (get_field('modal_type') == 'subscribe') {
        get_template_part( 'partials/subscribe', 'modal' );  
    }
} ?>


<?php get_footer(); ?>