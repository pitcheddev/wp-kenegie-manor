<?php get_header(); the_post();

      //vars
	  $unitgrades = get_field('unit_grades');
      $unitsize = get_field('unit_size');
      $unitimg = get_field('unit_gallery');
?>

	<section class="content-wrapper">
        <div class="accommodation-detail">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-8 text-center">
                        <h1><?php the_title(); ?></h1>
                        <p><?php the_field('unit_short_description') ?></p>
						<?php if(get_field('unit_price_band_flash')) {?>
							<span class="price-band"><?php the_field('unit_price_band_flash') ?></span>
						<?php } ?>
                        <span class="sleeps"><?php the_field('unit_size') ?></span>
						<?php if( in_array('pets', $unitgrades) ) { ?>
							<span class="pet-friendly"><i class="fas fa-paw"></i> Pet Friendly</span>
						<?php } ?>
                       
                    </div>
                </div>
            </div>

            <div class="gallery">
                <div class="container">
                    <div class="row">
						<?php foreach( $unitimg as $image ): ?>
							<div class="col-6 col-md-3">
								<a href="<?php echo $image['url']; ?>" rel = "lightbox">
									<img src="<?php echo $image['sizes']['home-accom']; ?>" alt="<?php echo $image['alt']; ?>">
								</a>
							</div>
						<?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 order-lg-2">
                        <div class="property-grades">
                            <span>Property Grades:</span>
                            <ul>
								<?php if( in_array('bronze', $unitgrades) ) { ?>
									<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/bronze.png" alt="Bronze"></li>
								<?php } ?>
                                <?php if( in_array('silver', $unitgrades) ) { ?>
									<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/silver.png" alt="Silver"></li>
								<?php } ?>
								<?php if( in_array('gold', $unitgrades) ) { ?>
									<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/gold.png" alt="Gold"></li>
								<?php } ?>
								<?php if( in_array('platinum', $unitgrades) ) { ?>
									<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/platinum.png" alt="Platinum"></li>
								<?php } ?>
                            </ul>
                        </div>
                        <div class="features">
                            <h3 class="title">Features at a glance</h3>
							<?php if( have_rows('unit_features_list') ){
								echo '<ul>';
								while( have_rows('unit_features_list') ){ the_row(); 
							?>
									<li><?php the_sub_field('unit_features_list_content') ?></li>
							<?php 
								};
								echo '</ul>';
							}; ?>
                            <a href="https://kenegie.searchbreaks.com" class="btn solid yellow" onclick="__gaTracker('send', 'event', 'Booking', 'Booking Initiated', 'Booking widget');">Book now</a>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <?php the_content(); ?>
                    </div>                    
                </div>
            </div>
			<?php
			$post_objects = get_field('unit_cross_sales');
			if( $post_objects ){ ?>
			<section class="cross-sales">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2>View what else we have to offer...</h2>
                        </div>
						<?php foreach( $post_objects as $post){ 
							setup_postdata($post);
							$unitgrades = get_field('unit_grades');
						?>
							<div class="col-lg-4">
								<div class="accom-wrapper">
									<div class="accom">
										<div class="price-flash">Short break from just <span>&pound;<?php the_field('unit_price_low') ?></span></div>
										<div class="img-wrapper">
											<img src="<?php echo get_the_post_thumbnail_url(null, 'offer-list') ?>" alt="">
										</div>
										<div class="content">
											<h3><?php the_title(); ?></h3>
											<h5><?php the_field('unit_sub_title') ?></h5>
											<?php if( in_array('pets', $unitgrades) ) { ?>
												<div class="pet-friendly"><i class="fas fa-paw"></i></div>
											<?php } ?>
											<span class="sleeps"><?php the_field('unit_size') ?></span>
											<?php if(get_field('unit_price_band_flash')) {?>
												<span class="price-band"><?php the_field('unit_price_band_flash') ?></span>
											<?php } ?>
											
											<?php if( have_rows('unit_features_list') ){
												echo '<ul>';
												$count = 0;
												while( have_rows('unit_features_list') ){ the_row(); 
											?>
													<li><?php the_sub_field('unit_features_list_content') ?></li>
											<?php 
													$count++;
													if($count == 3) break;
												};
												echo '</ul>';
											}; ?>
											<a href="<?php the_permalink() ?>" title="View <?php the_title(); ?>" class="btn outline violet">Take a look</a>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</section>
			<?php 
			} ?>
		</div>
	</section>
<?php get_footer(); ?>