<blockquote class="review">
  <div class="guest-say">Read what our guests say...</div>
                <?php
                  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                  query_posts(
                    array(
                      'post_type' => 'review',
                      'posts_per_page' => 1,
                      'order' => ASC,
                      'orderby' => rand,
                      'paged' => $paged )
                  );
                  if (have_posts()) :
                  while (have_posts()) :
                  the_post();

                the_content();

                endwhile;
                      endif;
                wp_reset_query();
                ?>
</blockquote>