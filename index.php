<?php get_header(); ?>
	<section class="content-wrapper">
        <div class="container">
			<div class="row">
				<div class="col-10 offset-1">
					<h1><?php single_post_title(); ?></h1>
					<div class="offers-page-wrapper">
						<?php
						$owners_category_id = get_cat_ID('owners');
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						query_posts(
							array(
								'category__not_in' => array($owners_category_id),
							  	'posts_per_page' => 12,
							  	'paged' => $paged 
							)
						);
						if (have_posts()) :
							while (have_posts()) : the_post();
							?>
						
								<div class="offer post">							
									<div class="row">
										<div class="col-md-4">
											<?php the_post_thumbnail('post-img'); ?>
										</div>
										<div class="col-md-8">
											<div class="content">
												<h2><?php the_title(); ?></h2>
												<p><?php excerpt(35); ?></p>
												<a href="<?php the_permalink(); ?>" class="btn solid light-violet">Read this article</a>
											</div>
										</div>
									</div>                        
								</div>

						<?php endwhile;
							  endif;
						?>
					</div>
					<div class="col-md-12">
                        <div class="navi">
                            <?php pagination_nav(); ?>                  
                        </div>
                    </div>
					 <?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>