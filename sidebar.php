	<div class="d-none d-lg-block col-lg-3">
		<aside class="sidebar">
			<?php //get_template_part('booking'); ?>
			
			<?php

			if (have_rows('sidebar_widgets')) {

				while (have_rows('sidebar_widgets')) { the_row();
				
					if( get_row_layout() == 'sidebarwidget_offer_list') { ?> 
					
						<div class="side-offers-wrapper">
							<div class="flash"></div>
							<div class="side-offers">
								<?php
								$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
								query_posts(
									array(
									  'post_type' => 'special_offer',
									  'posts_per_page' => 2,
									  'order' => ASC,
									  'orderby' => rand,
									  'paged' => $paged )
								  );
								  if (have_posts()) :
								  while (have_posts()) :
								  the_post();
								?>
									<div class="offer">
										<h2><?php the_title(); ?></h2>
										<h3><?php the_field('offer_sub_heading'); ?></h3>
									</div>
								 <?php endwhile;
									  endif;
								wp_reset_query();
								?>
								<a href="/special-offers/" class="btn solid light-violet">See all offers</a>
							</div>
						</div>
					<?php 
					}	

					else if( get_row_layout() == 'sidebarwidget_holiday_highlight' ) { ?> 
						
						<div class="holiday-highlights-block">
							<header class="title">Holiday highlights</header>
							<ul>
								<li>Beautiful setting in West Cornwall</li>
								<li>Close to top family attractions</li>
								<li>Wide range of accommodation</li>
								<li>Great on-site facilities</li>
								<li>Pet friendly units available</li>
								<li>Always value for money</li>
							</ul>
						</div>
					<?php 
					}	

					else if( get_row_layout() == 'sidebarwidget_latest_news' ) { ?> 

						<?php
							  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							  query_posts(
								array(
								  'posts_per_page' => 1,
								  'paged' => $paged )
							  );
							  if (have_posts()) :
							  while (have_posts()) : the_post();
								
								$attachment_id = get_post_thumbnail_id();
								$alt = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );
								if (strlen(trim($alt)) == 0){
									$alt = get_the_title();
								}
						?>
							<div class="latest-news">
								<img src="<?php echo get_the_post_thumbnail_url(null, 'accom-list') ?>" alt="<?= $alt ?>" />
								<h3><?php the_title(); ?></h3>
								<p><?php excerpt('20'); ?></p>
								<a href="<?php the_permalink(); ?>" class="btn solid light-violet w-100">Read Article</a>
							</div>
						<?php endwhile;
							  endif;
							wp_reset_query();
						?>
					<?php 
					}	

					else if( get_row_layout() == 'sidebarwidget_ready_explore' ) { ?> 

						<div class="explore">
							<img src="<?php echo get_template_directory_uri(); ?>/images/others/explore.png" alt="" />
							<h3>Get ready to explore!</h3>
							<p>Take a journey through the remote and wild landscape of West Cornwall and experience a region that is made for exploring. Visit the iconic St Michael’s Mount, Land’s End or take in the awe-inspiring beauty of the Lizard Peninsula.</p>
							<a href="/holidays-in-cornwall/" class="btn outline white w-80">Find out more</a>
						</div>
					<?php 
					}
				}
			}
			else{
			if(is_single() || is_page()) { ?> 
					
					<div class="side-offers-wrapper">
						<div class="flash"></div>
						<div class="side-offers">
							<?php
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts(
								array(
								  'post_type' => 'special_offer',
								  'posts_per_page' => 2,
								  'order' => ASC,
								  'orderby' => rand,
								  'paged' => $paged )
							  );
							  if (have_posts()) :
							  while (have_posts()) :
							  the_post();
							?>
								<div class="offer">
									<h2><?php the_title(); ?></h2>
									<h3><?php the_field('offer_sub_heading'); ?></h3>
								</div>
							 <?php endwhile;
								  endif;
							wp_reset_query();
							?>
							<a href="/special-offers/" class="btn solid light-violet">See all offers</a>
						</div>
					</div>
					<div class="holiday-highlights-block">
						<header class="title">Holiday highlights</header>
						<ul>
							<li>Beautiful setting in West Cornwall</li>
							<li>Close to top family attractions</li>
							<li>Wide range of accommodation</li>
							<li>Great on-site facilities</li>
							<li>Pet friendly units available</li>
							<li>Always value for money</li>
						</ul>
					</div>
				<?php 
				}	
			}?> 
		</aside>
	</div>