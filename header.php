<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title(' | ', true, 'right')?></title>
	<meta content="Kenegie Manor Holiday Park" name="author">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
	<script src="https://use.typekit.net/qii0bin.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	
	<!-- CSS Start -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<!-- CSS End -->
	
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag("consent", "default", {
        ad_storage: "denied",
        ad_user_data: "denied", 
        ad_personalization: "denied",
        analytics_storage: "denied",
        functionality_storage: "denied",
        personalization_storage: "denied",
        security_storage: "granted",
        wait_for_update: 2000,
    });
    gtag("set", "ads_data_redaction", true);
    gtag("set", "url_passthrough", true);
</script>
	<?php wp_head(); ?>
	<!-- Start Pitched Tracking -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W87GVWN');</script>
	<!-- End Pitched Tracking -->
	
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/@pitched/pitched-widget-standard@latest/dist/pitched-widget-standard.min.css">
	<style>
		.pitched-widget .pitched-widget-field select.pitched-widget-select, 
		.pitched-widget .pitched-widget-field input.pitched-widget-input {
			border: none!important;
		}
		.pitched-widget-select {
			margin: 0!important;	
		}
		.pitched-widget .pitched-widget-field input.pitched-widget-input {
			padding-top:15px!important;	
		}
		.pitched-widget .pitched-widget-field input.pitched-widget-input::placeholder {
			color: rgba(0,0,0,0.87)!important;
		}
		.pitched-widget-select:disabled {
			opacity: 1!important
		}
		.pitched-widget-submit {
			color: #8a4e8c!important;
			font-weight: bold;
		}
		
	</style>
</head>
<body <?php body_class(); ?>>
	<!-- Tracking (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W87GVWN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=519015464971918&ev=PageView&noscript=1"/></noscript>
	<!-- End Tracking (noscript) -->
	<div id="mmwrapper"> 
		<div id="mob-menu">
			<div class="row">
				<div class="col-12">
					<div class="logo">
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/mob-logo.png" alt=""></a>
					</div>
					<a href="tel:08000436449" class="phone"><i class="fas fa-phone fa-flip-horizontal"></i> Call</a>
					<a href="/cornwall-holiday-park/directions/" class="location"><i class="fas fa-map-marker-alt"></i>Find us</a>
					<a href="https://kenegie.searchbreaks.com" class="book"><i class="fas fa-calendar-alt"></i> Book</a>

					<a href="#mmenu" class="menu d-inline-block my-2 mx-2"><i class="fal fa-bars"></i></a>  
				</div>
			</div>
		</div>
		<div id="wrapper">
				
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/kenegie-logo.png" alt="Kenegie Manor" /></a>
					</div>
					<div class="row">            
						<div class="col-md-5 col-xl-5 offset-md-2 offset-xl-2">
						  <a href="/cornwall-holiday-park/brochure-request/">Request a brochure</a>
						  <img src="<?= get_template_directory_uri() . '/images/TC.png' ?>" alt="Trip advisor" width="91" height="100" />
						</div>
						<div class="col-md-3">
						  <div class="call">
							Call the booking hotline <span>0800 043 6449</span>
						  </div>
						</div>
						<div class="col-md-2">
						  <a href="https://kenegie.searchbreaks.com" class="btn book">Click to book online today</a>
						</div>
					</div>
				</div>
			</header>
			
			<nav id="main-nav">
				<div class="container">
					<div class="row">
						<div class="col-md-11 offset-md-1 text-right">
							<?php kenegie_get_header_menu(); ?>
						</div>
					</div>             
				</div>
			</nav>
			<?php get_template_part('includes', 'slider');  ?>
			
			<?php get_template_part('includes', 'bookingwidget');  ?>