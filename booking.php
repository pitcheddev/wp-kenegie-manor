<div class="booking-widget-wrapper">
  <?php if (is_front_page()) {
    echo '<div class="container">';
  } 
  wp_enqueue_script('pitchedbooking');
  ?>
        <div class="booking-widget">
			<h2>Check prices and book online...</h2>
			<!-- TEMPORARY HIDE
						
            Online booking is currently undergoing upgrade and is temporarily unavailable, please call our booking team on<br />
            <h2>0800 043 6449</h2>	
			 END TEMPORARY HIDE -->
			 
			<form action="https://kenegie.searchbreaks.com/Booking/CheckAvailabilty" method="post"> 
                <input type="hidden" id="HolidayTypeIDMob" name="HolidayTypeID" value="1">
                <input id="ArrivalDateMob" name="ArrivalDate" readonly="true" type="text" />
                <select data-val="true" data-val-number="The field Duration must be a number." data-val-required="The Duration field is required." id="DurationMob" name="Duration">
                    <option selected="selected" value="">Select Duration</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                </select>
                <div>
                    <div class="left">
                    	<select data-val="true" data-val-number="The field Adults must be a number." data-val-required="The Adults field is required." id="AdultsMob" name="Adults">
                    		<option selected="selected" value="">Adults</option>
                    		<option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                    
                    <div class="right">
                    	<select data-val="true" data-val-number="The field Children must be a number." data-val-required="The Children field is required." id="ChildrenMob" name="Children">
                    		<option selected="selected" value="0">Children</option>
                            <option>0</option>
                    		<option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                </div>    
                <input type="submit" value="Search" class="btn" onclick="__gaTracker('send', 'event', 'Booking', 'Booking Initiated', 'Booking widget');">
            </form>
           
        </div>
      <?php if(is_front_page()) {
        echo '</div>';
      } ?>
    </div>