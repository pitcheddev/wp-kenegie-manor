<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" data-display-frequency="<?php the_field('modal_display_frequency'); ?>" data-opening-delay="<?php the_field('modal_opening_delay') ?>">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row w-100">
          <div class="col-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="text-white" aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="col-10">
              <h3 class="modal-title text-center text-white"><?php the_field('modal_title') ?></h3>
          </div>
        </div>
      </div>
      <div class="modal-body">
          <div class="row text-center text-white w-100">
            <div class="col-12 w-100">
                <h6><?php the_field('modal_information'); ?></h6>
            </div>
          </div>
          <div class="row text-center w-100">
              <a class="btn confirm-btn" href="<?php echo get_field('modal_button')['URL']; ?>"><?php echo get_field('modal_button')['label']; ?></a>
          </div>
        </div>
    </div>
  </div>
</div>