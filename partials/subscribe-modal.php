<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" data-display-frequency="<?php the_field('modal_display_frequency'); ?>" data-opening-delay="<?php the_field('modal_opening_delay') ?>">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row w-100">
          <div class="col-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="text-white" aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="col-10">
              <h3 class="modal-title text-center text-white"><?php the_field('modal_title') ?></h3>
          </div>
        </div>
      </div>
      <div class="modal-body">
          <div class="row text-center text-white w-100">
                <?php gravity_form( 7, false, false, false, null, true, 2000, true ); ?>
          </div>
          <div class="row text-center w-100">
                <p class="text-light text-center">By submitting this form you are agreeing to receive marketing communications from Kenegie Manor. To find out more about our privacy policy and what information we will send you <a href="https://www.kenegie-manor.co.uk/data-protection/" target="_blank">click here</a></p>
          </div>
        </div>
    </div>
  </div>
</div>